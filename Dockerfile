# Use Python 3.9 slim as the base image
ARG BASE_IMAGE=python:3.9-slim
FROM $BASE_IMAGE as runtime-environment

# Install system dependencies and Java
USER root
RUN mkdir -p /var/lib/apt/lists/partial && \
    apt-get update && \
    apt-get install -y default-jdk && \
    rm -rf /var/lib/apt/lists/*

# Install Python dependencies
COPY requirements.txt /tmp/requirements.txt
RUN python -m pip install -U "pip>=21.2,<23.2"
RUN pip install --no-cache-dir -r /tmp/requirements.txt && rm -f /tmp/requirements.txt

# Add kedro user
ARG KEDRO_UID=999
ARG KEDRO_GID=0
RUN groupadd -f -g ${KEDRO_GID} kedro_group && \
    useradd -m -d /home/kedro_docker -s /bin/bash -g ${KEDRO_GID} -u ${KEDRO_UID} kedro_docker

WORKDIR /home/kedro_docker
USER kedro_docker

# Copy the project files except what's in .dockerignore
COPY --chown=${KEDRO_UID}:${KEDRO_GID} . .

# Install Flask and any other dependencies listed in requirements.txt
USER root
RUN pip install Flask

# Expose port 5002 for the Flask app
EXPOSE 5002

# Set the entry point to run the Flask app
CMD ["python", "app.py"]
