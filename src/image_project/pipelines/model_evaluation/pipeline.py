# src/image_project/pipelines/model_evaluation/pipeline.py

from kedro.pipeline import Pipeline, node
from .nodes import evaluate_model

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=evaluate_model,
                inputs=["params:evaluation.results_path"],
                outputs="evaluation_metrics",
                name="evaluate_model_node",
            ),
        ]
    )
