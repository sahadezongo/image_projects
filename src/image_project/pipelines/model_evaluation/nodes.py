# src/image_project/pipelines/model_evaluation/nodes.py

import pandas as pd

def evaluate_model(results_path: str):
    try:
        # Read the results CSV file
        results = pd.read_csv(results_path)

        # Strip whitespace from column names
        results.columns = results.columns.str.strip()

        # Extract the relevant metrics (adjust column names as necessary)
        precision = results["metrics/precision"].iloc[-1]
        recall = results["metrics/recall"].iloc[-1]
        mAP_0_5 = results["metrics/mAP_0.5"].iloc[-1]
        mAP_0_5_0_95 = results["metrics/mAP_0.5:0.95"].iloc[-1]

        # Create a dictionary of the evaluation metrics
        evaluation_metrics = {
            "precision": precision,
            "recall": recall,
            "mAP_0.5": mAP_0_5,
            "mAP_0.5:0.95": mAP_0_5_0_95,
        }

        return evaluation_metrics  # Return as a list


    except KeyError as e:
        raise KeyError(f"Failed to find expected column in results CSV: {e}")

    except Exception as e:
        raise RuntimeError(f"An error occurred during evaluation: {e}")
