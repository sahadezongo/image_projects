import torch
from shutil import copyfile

from ..yolov5 import train


def train_yolo(data_yaml: str, model_path: str, epochs: int = 5):

    # Arguments pour l'entraînement
    args = {
        'data': data_yaml,
        'img': 512,
        'batch': 8,
        'epochs': epochs,
        'weights': 'yolov5s.pt',
    }

    # Lancer l'entraînement
    train.run(**args)
