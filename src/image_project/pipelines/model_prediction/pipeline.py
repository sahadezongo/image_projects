# src/image_project/pipelines/model_creation/pipeline.py

from kedro.pipeline import Pipeline, node
from .nodes import train_yolo

def create_pipeline(**kwargs):
    return Pipeline(
        [
            node(
                func=train_yolo,
                inputs=["params:train.data_yaml", "params:train.model_path", "params:train.epochs"],
                outputs=None,
                name="train_yolo_node",
            ),
        ]
    )