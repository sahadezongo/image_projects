"""Project pipelines."""
from __future__ import annotations

from typing import Dict

from kedro.framework.project import find_pipelines
from kedro.pipeline import Pipeline

# Importez votre pipeline model_prediction
from image_project.pipelines.model_prediction import create_pipeline as create_model_prediction_pipeline
from image_project.pipelines.model_evaluation import create_pipeline as create_model_evaluation_pipeline


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from pipeline names to Pipeline objects.
    """
    # Créez une instance du pipeline model_prediction
    model_prediction_pipeline = create_model_prediction_pipeline()

    # Create an instance of the model_evaluation pipeline
    model_evaluation_pipeline = create_model_evaluation_pipeline()
    
    # Ajoutez-le au dictionnaire des pipelines
    pipelines = {
        "model_prediction": model_prediction_pipeline,
        "model_evaluation": model_evaluation_pipeline
    }
    
    # Concatenate all pipelines (s'il y en a d'autres à concaténer)
    all_pipelines = sum(pipelines.values(), Pipeline([]))

    # Define "__default__" pipeline
    pipelines["__default__"] = all_pipelines
    return pipelines
