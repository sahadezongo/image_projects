from flask import Flask, request, jsonify, render_template, redirect, url_for
from pathlib import Path
import torch
from PIL import Image
import io
import os

app = Flask(__name__)

# Load the pre-trained YOLOv5 model
model_path = "src/image_project/pipelines/yolov5/runs/train/exp16/weights/best.pt"
model = torch.hub.load('ultralytics/yolov5', 'custom', path=model_path)

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/upload_predict', methods=['GET'])
def upload_predict():
    return render_template('upload_predict.html')

@app.route('/predict', methods=['POST'])
def predict():
    try:
        # Get the uploaded file
        file = request.files['file']
        if not file:
            return redirect(url_for('home', error="No file uploaded"))

        # Read the image
        img_bytes = file.read()
        img = Image.open(io.BytesIO(img_bytes))

        # Make predictions
        results = model(img)

        # Process results
        predictions = results.pandas().xyxy[0].to_dict(orient="records")

        return jsonify(predictions)
    except Exception as e:
        return redirect(url_for('home', error=str(e)))

if __name__ == "__main__":
    app.run(host='127.0.0.1', port=5002, debug=True)
